# Sorting Algorithms

Implementation of various sorting algorithms using .txt files as input

# Files

mergeSort.py: implementation of the merge sort algorithm

quickSort.py: implementation of the quickSort algorithm

insertionSort.py: implementation of the insertion sort algorithm


