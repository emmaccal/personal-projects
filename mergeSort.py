import time
import matplotlib.pyplot as plt
import numpy as np

    
def mergeSort(given_array):
#mergeSort implements the mergeSort algorithm to sort an array
#array is any given array we would like to sort
    
    
    #checks to make sure array is of valid size
    if len(given_array) > 1: 
        middle_of_array = len(given_array)//2
        left_half_of_array = given_array[:middle_of_array] 
        right_half_of_array = given_array[middle_of_array:]

        #call mergeSort on relevant half
        mergeSort(left_half_of_array) 
        mergeSort(right_half_of_array)

        #assign all variables a value of 0
        i = j = k = 0
        
        #moves data to correct arrays
        while i < len(left_half_of_array) and j < len(right_half_of_array): 
            if left_half_of_array[i] < right_half_of_array[j]: 
                given_array[k] = left_half_of_array[i] 
                i+=1
            else: 
                given_array[k] = right_half_of_array[j] 
                j+=1
            k+=1
        
        #add elements back into the array 
        while i < len(left_half_of_array): 
            given_array[k] = left_half_of_array[i] 
            i+=1
            k+=1
          
        while j < len(right_half_of_array): 
            given_array[k] = right_half_of_array[j] 
            j+=1
            k+=1

    return given_array

def numbersToSort(filename):
    #numbersToSort reads a file and stores in array to be used in function calls
    #filename is filename of the file to sort
    array = []
    with open(filename) as f:
        for line in f:
            array.append(line.split(" "))  
    array = array[0][1:]
    array = list(map(int, array)) 
    return array

if __name__ == "__main__":
    file_input = ("1000.txt","5000.txt","10000.txt","50000.txt")
    merge_sort_runtimes = []

    print("RUNTIMES FOR MERGE SORT")
    #for loop to read through file input
    for i in file_input:
        #call numbersToSort on the file_input list
        numbers_to_sort = numbersToSort(i)
        #record start time
        start_time = time.time()
        #call mergeSort
        z = mergeSort(numbers_to_sort)
        #record end time
        end_time = time.time()
        #calculate total time
        total_time = end_time - start_time
        #print the runtime of each file
        print(f"The runtime for {i} is {total_time}")
        print("---------------------------------")
        #append the total time into appropiate list 
        merge_sort_runtimes.append(total_time)

    #Label x-axis
    plt.xlabel("File Name")
    #Label y-axis
    plt.ylabel("Runtimes(seconds)")
    #Name graph
    plt.title("Runtime Graph")
    
    #Plotting the x-axis 
    x1 = np.array([0,1,2,3])
    x1_ticks = ["1000.txt","5000.txt","10000.txt","50000.txt"]

    y1 = merge_sort_runtimes

    plt.xticks(x1,x1_ticks)

    #Plot both x and y
    plt.plot(x1,y1,label="mergeSort")

    plt.legend()
    #Show the graph
    plt.show()