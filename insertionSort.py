import time
import matplotlib.pyplot as plt
import numpy as np


def insertionSort(given_array):
    for i in range(1, len(given_array)):
        given_number = given_array[i]
        k = i - 1
        while k >= 0 and given_number < given_array[k]:
            given_array[k+1] = given_array[k]
            k -= 1
        given_array[k+1] = given_number

    return given_array

def numbersToSort(filename):
    #numbersToSort reads a file and stores in array to be used in function calls
    #filename is filename of the file to sort
    array = []
    with open(filename) as f:
        for line in f:
            array.append(line.split(" "))  
    array = array[0][1:]
    array = list(map(int, array)) 
    return array

if __name__ == "__main__":
    file_input = ("1000.txt","5000.txt","10000.txt","50000.txt")
    insertion_sort_runtimes = []

    #for loop to read through file input
    for i in file_input:
        #call numbersToSort on the file_input list
        numbers_to_sort = numbersToSort(i)
        #record start time
        start_time = time.time()
        #call insertionSort on the numbers in file
        z = insertionSort(numbers_to_sort)
        #record end time
        end_time = time.time()
        #record total time to run the function
        total_time = end_time - start_time
        #print the runtime for each file
        print(f"The runtime for {i} is {total_time}")
        print("---------------------------------")        
        #append the total time into the runtime list
        insertion_sort_runtimes.append(total_time)

    #This section is for plotting the runtime graph

    #Label x-axis
    plt.xlabel("File Name")
    #Label y-axis
    plt.ylabel("Runtimes(seconds)")
    #Name graph
    plt.title("Runtime Graph")
    
    #Plotting the x-axis 
    x1 = np.array([0,1,2,3])
    x1_ticks = ["1000.txt","5000.txt","10000.txt","50000.txt"]
    y1 = insertion_sort_runtimes
  
    #Connect x_ticks and x
    plt.xticks(x1,x1_ticks)
   
    #Plot both x and y
    plt.plot(x1,y1,label="insertionSort")
   
    plt.legend()
    #Show the graph
    plt.show()
    