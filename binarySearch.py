import time
import matplotlib.pyplot as plt
import numpy as np
import random

#function to search an array linearly
def linearSearch(given_array, chosen_value):
    """
    linearSearch takes an array and a given value
    given_array represents the array
    chosen_value represents the given value to search for
    """
    for i in range(0, len(given_array)):
        #for loop to search through array in range (0, to the end of the array)
        if (given_array[i] == chosen_value):
            #if the current value is given value, return location of given current value
            return i
    #if value is not in array, return -1
    return -1

def binarySearch(given_array, left, right, element):
    if right >= left: 
        middle = left + (right - left) // 2
        if given_array[middle] == element:
            return middle
        elif given_array[middle] > element:
            return binarySearch(given_array,left, middle -1, element)
        
        else:
            return binarySearch(given_array, middle + 1, right, element)

    else:
        print("Element Not Present")

#function to create an array of any size filled with random numbers in a given range
def randomArrayGenerator(start, end, number_of_elements):
    #create an array
    array = []
    #for loop to search through a given range
    for j in range(number_of_elements):
        #append the element into the array
        #random.randint gives an integer in given range
        array.append(random.randint(start, end)) 
    #return the array
    return array

def numbersToSort(filename):
    #numbersToSort reads a file and stores in array to be used in function calls
    #filename is filename of the file to sort
    array = []
    with open(filename) as f:
        for line in f:
            array.append(line.split(" "))  
    array = array[0][1:]
    array = list(map(int, array)) 
    return array

if __name__ == "__main__":
    # file_input = ("1000.txt","5000.txt","10000.txt","50000.txt")
    # merge_sort_runtimes = []

    # print("RUNTIMES FOR MERGE SORT")
    # #for loop to read through file input
    # for i in file_input:
    #     #call numbersToSort on the file_input list
    #     numbers_to_sort = numbersToSort(i)
    #     #record start time
    #     start_time = time.time()
    #     #call mergeSort
    #     z = mergeSort(numbers_to_sort)
    #     #record end time
    #     end_time = time.time()
    #     #calculate total time
    #     total_time = end_time - start_time
    #     #print the runtime of each file
    #     print(f"The runtime for {i} is {total_time}")
    #     print("---------------------------------")
    #     #append the total time into appropiate list 
    #     merge_sort_runtimes.append(total_time)

    size_n = [16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576]
    
    
    print("USING input_100.txt file")
    #call getSearchKeys using first input file
    keys = numbersToSort("100.txt")

    #create a for loop to loop through size_of_n array
    for size in size_n:
        #create an array with randomArrayGenerator that has values 0 to n, and is of size n 
        array_of_n = randomArrayGenerator(0, size, size)
        #create a variable total time and set equal to zero
        total_time = 0
        #for loop to loop through keys
        for i in keys:
            #take start time
            start_time = time.time()
            #call linearSearch with array of size n and chosen search key
            z = linearSearch(array_of_n, i)
            #take end time of program
            end_time = time.time()
            #calculate total runtime 
            run_time = end_time - start_time
            #add run_time to total_time
            total_time += run_time
        print(f"LINEAR SEARCH: The total time for an array of size {size}: ", total_time)
    
    
        total_time = 0
        for i in keys:
            #copy above array into array_copy
            array_copy = array_of_n[:]
            #initalize start time
            start_time = time.time()
            #call built-in sort function
            array_copy.sort()
            s = binarySearch(array_copy, 0, len(array_copy)-1, i)
            end_time = time.time()
            run_time = end_time - start_time
            total_time += run_time
            #print(f"search key: {i}, run time: {run_time}")
        print(f"BINARY SEARCH: The total time for an array of size {size}: ", total_time)

   
    #Repeat above steps using the second input file
    print("USING input_1000.txt file")

    keys = numbersToSort("1000.txt")
    for size in size_n:
        array_of_n = randomArrayGenerator(0, size, size)
        total_time = 0
        for i in keys:
            start_time = time.time()
            z = linearSearch(array_of_n, i)
            end_time = time.time()
            run_time = end_time - start_time
            total_time += run_time
        print(f"LINEAR SEARCH: The total time for an array of size {size}: ", total_time)

        total_time = 0
        for i in keys:
            array_copy = array_of_n.copy()
            start_time = time.time()
            array_copy.sort()
            s = binarySearch(array_copy, 0, len(array_copy)-1, i)
            end_time = time.time()
            run_time = end_time - start_time
            total_time += run_time
        print(f"BINARY SEARCH: The total time for an array of size {size}: ", total_time)
 

    # #Label x-axis
    # plt.xlabel("File Name")
    # #Label y-axis
    # plt.ylabel("Runtimes(seconds)")
    # #Name graph
    # plt.title("Runtime Graph")
    
    # #Plotting the x-axis 
    # x1 = np.array([0,1,2,3])
    # x1_ticks = ["1000.txt","5000.txt","10000.txt","50000.txt"]

    # y1 = merge_sort_runtimes

    # plt.xticks(x1,x1_ticks)

    # #Plot both x and y
    # plt.plot(x1,y1,label="mergeSort")

    # plt.legend()
    # #Show the graph
    # plt.show()