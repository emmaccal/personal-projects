import time
import matplotlib.pyplot as plt
import numpy as np

def choosePartition(given_array, low, high):
    i = (low - 1)
    pivot = given_array[high]

    for i in range(low,high):
        if given_array[j] <= pivot:
            i = i + 1
            given_array[i],given_array[j] = given_array[j],given_array[i]
    given_array[i+1], given_array[high] = given_array[high], given_array[i + 1]
    return (i+1)

def quickSort(given_array,low,high):
    if len(given_array) == 1:
        return given_array
    if low < high:
        chosen_pivot = choosePartition(given_array,low,high)
        quickSort(given_array, low, chosen_pivot - 1)
        quickSort(given_array, chosen_pivot + 1, high)

def numbersToSort(filename):
    #numbersToSort reads a file and stores in array to be used in function calls
    #filename is filename of the file to sort
    array = []
    with open(filename) as f:
        for line in f:
            array.append(line.split(" "))  
    array = array[0][1:]
    array = list(map(int, array)) 
    return array

if __name__ == "__main__":
    file_input = ("1000.txt","5000.txt","10000.txt","50000.txt")
    quick_sort_runtimes = []

    print("RUNTIMES FOR MERGE SORT")
    #for loop to read through file input
    for i in file_input:
        #call numbersToSort on the file_input list
        numbers_to_sort = numbersToSort(i)
        #record start time
        start_time = time.time()
        #call mergeSort
        z = quickSort(numbers_to_sort,0,len(numbers_to_sort))
        #record end time
        end_time = time.time()
        #calculate total time
        total_time = end_time - start_time
        #print the runtime of each file
        print(f"The runtime for {i} is {total_time}")
        print("---------------------------------")
        #append the total time into appropiate list 
        quick_sort_runtimes.append(total_time)

    #Label x-axis
    plt.xlabel("File Name")
    #Label y-axis
    plt.ylabel("Runtimes(seconds)")
    #Name graph
    plt.title("Runtime Graph")
    
    #Plotting the x-axis 
    x1 = np.array([0,1,2,3])
    x1_ticks = ["1000.txt","5000.txt","10000.txt","50000.txt"]

    y1 = merge_sort_runtimes

    plt.xticks(x1,x1_ticks)

    #Plot both x and y
    plt.plot(x1,y1,label="quickSort")

    plt.legend()
    #Show the graph
    plt.show()


